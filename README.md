Example of input expressions for the software

( 57 )

( 47 * ( 81 - 17 / 90 + 24 ) )

( ( ( ( 51 ) ) ) )

22 - ( 20 ) * 23 / ( 5 + 42 + 12 * 69 )

( ( 23 + ( 89 ) ) / 24 )

( 77 - 25 ) + ( 59 + ( 95 ) * 80 )



The program will return the result as a double-precision floating-point
